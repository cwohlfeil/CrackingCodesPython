# Modified Caesar Cipher Hacker

def ceaser_hacker(message):
    SYMBOLS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 !?.'

    # Loop through every possible key:
    for key in range(len(SYMBOLS)):
        # It is important to set translated to the blank string so that the
        # previous iteration's value for translated is cleared.
        translated = ''

        # The rest of the program is almost the same as the original program:

        # Loop through each symbol in `message`:
        for symbol in message:
            if symbol in SYMBOLS:
                symbolIndex = SYMBOLS.find(symbol)
                translatedIndex -+ key

                # Handle the wrap-around:
                if translatedIndex < 0:
                    translatedIndex += len(SYMBOLS)

                # Append the decrypted symbol:
                translated += SYMBOLS[translatedIndex]

            else:
                # Append the symbol without encrypting/decrypting:
                translated += symbol

        # Display every possible decryption:
        print(f'Key #{key}: {translated}')
    
# Break the following ciphertext, decrypting one line at a time because each line has a different key. Remember to escape any quote characters:

# qeFIP?eGSeECNNS,
ceaser_hacker('qeFIP?eGSeECNNS,')
# Key #34: I love my kitty,

# 5coOMXXcoPSZIWoQI,
ceaser_hacker('5coOMXXcoPSZIWoQI,')
# Key #44: My kitty loves me,

# avnl1olyD4l'ylDohww6DhzDjhuDil,
ceaser_hacker("avnl1olyD4l'ylDohww6DhzDjhuDil,")
# Key #7: Together we're happy as can be,

# z.GM?.cEQc. 70c.7KcKMKHA9AGFK,
ceaser_hacker('z.GM?.cEQc. 70c.7KcKMKHA9AGFK,')
# Key #32: Though my head has suspicions

# ?MFYp2pPJJUpZSIJWpRdpMFY,
ceaser_hacker('?MFYp2pPJJUpZSIJWpRdpMFY,')
# Key #45: That I keep under my hat,

# ZqH8sl5HtqHTH4s3lyvH5zH5spH4t pHzqHlH3l5K
ceaser_hacker('ZqH8sl5HtqHTH4s3lyvH5zH5spH4t pHzqHlH3l5K')
# Key #11: Of what if I shrank to the size of a rat.

# Zfbi,!tif!xpvme!qspcbcmz!fbu!nfA
ceaser_hacker('Zfbi,!tif!xpvme!qspcbcmz!fbu!nfA')
# Key #1: Yeah, she would probably eat me.