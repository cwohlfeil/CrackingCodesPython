# 1) If you assign spam = 'Cats', what do the following lines print?
spam = 'Cats'
spam + spam + spam
print(spam * 3) # Cats Cats Cats

# 2) What do the following lines print?
print("Dear Alice,\nHow are you?\nSincerely,\nBob") 
# Dear Alice,
# How are you?
# Sincerely,
# Bob

print('Hello' + 'Hello') # HelloHello

# 3) If you assign spam = 'Four score and seven years is eighty seven years.', what would each of the following lines print?
spam = 'Four score and seven years is eighty seven years.'
print(spam[5]) # s
print(spam[-3]) # r
print(spam[0:4] + spam[5]) # Fours
print(spam[-3:-1]) # rs
print(spam[:10]) # Four score
print(spam[-5:]) # ears.
print(spam[:]) # Four score and seven years is eighty seven years.

# 4) Which window displays the >>> prompt, the interactive shell or the file editor?

# 5) What does the following line print?
# print('Hello, world!') # Nothing, it's a comment