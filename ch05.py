# Modified Caesar Cipher
def ceaser_cipher(mode, message, key): 
    # Mode: Whether the program encrypts or decrypts, set to either 'encrypt' or 'decrypt'.
    # Message: The string to be encrypted/decrypted.
    # Key: The encryption/decryption key.

    # Every possible symbol that can be encrypted:
    SYMBOLS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 !?.'

    # Stores the encrypted/decrypted form of the message:
    translated = ''

    for symbol in message:
        # Note: Only symbols in the `SYMBOLS` string can be encrypted/decrypted.
        if symbol in SYMBOLS:
            symbolIndex = SYMBOLS.find(symbol)

            # Perform encryption/decryption:
            if mode == 'encrypt':
                translatedIndex += key
            elif mode == 'decrypt':
                translatedIndex -= key

            # Handle wrap-around, if needed:
            if translatedIndex >= len(SYMBOLS):
                translatedIndex -= len(SYMBOLS)
            elif translatedIndex < 0:
                translatedIndex += len(SYMBOLS)

            translated += SYMBOLS[translatedIndex]
        else:
            # Append the symbol without encrypting/decrypting:
            translated += symbol

    # Output the translated string:
    return translated


# Using caesarCipher.py, encrypt the following sentences with the given keys:
# '"You can show black is white by argument," said Filby, "but you will never convince me."' with key 8

print(ceaser_cipher('encrypt', '"You can show black is white by argument," said Filby, "but you will never convince me."', 8))
# "7w3ekive1pw5ejtikseq1e5pq2mej7eizo3umv2,"e1iqlenqtj7,e"j32e7w3e5qttevm4mzekwv4qvkmeumh"

# '1234567890' with key 21

print(ceaser_cipher('encrypt', '1234567890', 21))
# HIJKLMNOPQ


# Using caesarCipher.py, decrypt the following ciphertexts with the given keys:
# 'Kv?uqwpfu?rncwukdng?gpqwijB' with key 2

print(ceaser_cipher('decrypt', 'Kv?uqwpfu?rncwukdng?gpqwijB', 2))
# It sounds plausible enough.

# 'XCBSw88S18A1S 2SB41SE .8zSEwAS50D5A5x81V' with key 22

print(ceaser_cipher('decrypt', 'XCBSw88S18A1S 2SB41SE .8zSEwAS50D5A5x81V', 22))
# But all else of the world was invisible.

# Which Python instruction would import a module named watermelon.py?
# import watermelon


# What do the following pieces of code display on the screen?

spam = 'foo'
for i in spam:
    spam += i
print(spam)
# foofoo

if 10 < 5:
    print('Hello')
elif False:
    print('Alice')
elif 5 != 5:
    print('Bob')
else:
    print('Goodbye')
# Goodbye   
    
print('f' not in 'foo')
# False

print('foo' in 'f')
# False

print('hello'.find('oo'))
# -1