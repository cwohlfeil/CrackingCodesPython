# 1) What does the following piece of code print to the screen?
print(len('Hello') + len('Hello')) # 10

# 2) What does this code print?
i = 0
while i < 3:
    print('Hello')
    i += 1
# Hello
# Hello
# Hello
    
# 3) How about this code?
i = 0
spam = 'Hello'
while i < 5:
    spam += spam[i]
    i += 1
print(spam)
# Hello + H + e + l + l + o
# HelloHello

# 4) And this?
i = 0
while i < 4:
    while i < 6:
        i += 2
        print(i)
# 2
# 4
# 6