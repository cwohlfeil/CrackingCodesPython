"""
The steps for encrypting with the transposition cipher are as follows:
1. Count the number of characters in the message and the key.
2. Draw a row of a number of boxes equal to the key (for example, 8 boxes for a key of 8).
3. Start filling in the boxes from left to right, entering one character per box.
4. When you run out of boxes but still have more characters, add another row of boxes.
5. When you reach the last character, shade in the unused boxes in the last row.
6. Starting from the top left and going down each column, write out the characters. When you get to the bottom of a column, move to the next column to the right. Skip any shaded boxes. This will be the ciphertext.
"""

def encrypt_message(key, message):
    # Each string in ciphertext represents a column in the grid.
    ciphertext = [''] * key

    # Loop through each column in ciphertext.
    for column in range(key):
        currentIndex = column

        # Keep looping until currentIndex goes past the message length.
        while currentIndex < len(message):
            # Place the character at currentIndex in message at the
            # end of the current column in the ciphertext list.
            ciphertext[column] += message[currentIndex]

            # move currentIndex over
            currentIndex += key

    # Convert the ciphertext list into a single string value and return it.
    return ''.join(ciphertext)


# With paper and pencil, encrypt the following messages with the key 9 using the transposition cipher. The number of characters has been provided for your convenience.

# Underneath a huge oak tree there was of swine a huge company, (61 characters)
""" 
Key 9, * for spaces, # for empty boxes
U n d e r n e a t 
h * a * h u g e * 
o a k * t r e e * 
t h e r e * w a s 
* o f * s w i n e 
* a * h u g e * c 
o m p a n y , # #
"""
print(encrypt_message(9, 'Underneath a huge oak tree there was of swine a huge company,'))
# Uhot  on ahoamdakef pe  r harhtesunnur wgyegewie,aeean t  sec

# That grunted as they crunched the mast: For that was ripe and fell full fast. (77 characters)
print(encrypt_message(10, 'That grunted as they crunched the mast: For that was ripe and fell full fast.'))
# Te tFadlhdchos  a rer fftau  rea snmtilsg cahpltrthsae .uhett fned: auty  wnl

# Then they trotted away for the wind grew high: One acorn they left, and no more might you spy. (94 characters)
print(encrypt_message(11, 'Then they trotted away for the wind grew high: One acorn they left, and no more might you spy.'))
# Tr nhn,ruhofd:  e eto  ta sntrgOhnmp e rnediytdteey g.h hw  nheae alotyw hce   awiofmytyigrtoo


# In the following program, is each spam a global or local variable?
spam = 42
def foo():
    global spam
    spam = 99
    print(spam)
# Global, a variable cannot be both, and `global spam` is declared


# What value does each of the following expressions evaluate to?
[0, 1, 2, 3, 4][2]
# 2
[[1, 2], [3, 4]][0]
# [1, 2]
[[1, 2], [3, 4]][0][1]
# 2
['hello'][0][1]
# 'e'
[2, 4, 6, 8, 10][1:3]
# [4, 6]
list('Hello world!')
# ['H', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd', '!']
list(range(10))[2]
# 2


# What value does each of the following expressions evaluate to?
len([2, 4])
# 2
len([])
# 0
len(['', '', ''])
# 3
[4, 5, 6] + [1, 2, 3]
# [4, 5, 6, 1, 2, 3]
3 * [1, 2, 3] + [9]
# [1, 2, 3, 1, 2, 3, 1, 2, 3, 9]
42 in [41, 42, 42, 42]
# True


# What are the four augmented assignment operators?
# +=, -=, *=, /=